# Git
Git is a VCS (Version Control System) software which is used to track changes in a project. This will make collaboration with teammates simpler and faster.

## Git Workflow
![Git Workflow](https://s3-us-west-2.amazonaws.com/dgw-blog/2018/05/GIt-Workflow-Diagram--5-.png)

This image shows the basic workflow of git.



## Some Terminologies of Git
| Name           | Definition     |
| :------------- | :----------- |
| Repository | Often called as a repo. A repository is the collection of files and folders (code files) that you’re using git to track.   |
| Commit   | This is the command used to save your work in the local machine |
| Push | This command is used to upload your changes to the remote repository |
| Pull | This command is used to sync the local repository with the remote repository |
| Branch | A repo can have many branches. These branches are separate instances of code that is different from the main codebase |
| Merge | Merging is basically integrating two branches together |
| Clone | Cloning a repo is like downloading an entire repository on your local machine |
| Fork | Forking is similar to cloning instead forking will make a duplicate of entire repo under your name|



## Getting Started With Git
1. Install Git for your operating system by following the instructions [here](https://git-scm.com/).
1. Open terminal or command-line and execute `git clone <link-to-repo>`
2. After cloning the repo execute `cd <reponame>` to get inside the repo directory.
3. Create a new branch. This branch will be used to make your changes only
    - Execute `git checkout master`
    - Execute `git checkout -b <your-branch-name>`
4. Make changes to the project as you like
5. After making the changes execute `git add .` This command will allow git to track new files. The `.` stands for `all` and can be replaced with the file or folder name.
6. Now execute `git commit -m "<message>"` where message is the brief description of your commit.
7. By now the changes you made are only saved locally. Execute `git push origin <your-branch-name>` to push your changes in the remote repository.

You can raise a pull request to merge your branch with the main branch after the moderators review your code.

