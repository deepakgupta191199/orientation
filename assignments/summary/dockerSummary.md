# Docker
Docker is a program used to develop and run programs in a container. It can be used to simulate various environments and deplo(y our product without worrying about the dependencies.

## Docker Workflow
![DockerWorkflow](https://collabnix.com/wp-content/uploads/2015/10/WithoutDocker_workflow.jpg)

This image shows the basic docker workflow which is used to deploy an application.

## Some Terminologies of Docker
| Name           | Definition    |
| :------------- | :----------- |
| Docker | It is the program used to develop and run applications with containers. |
| Docker Image | This is the file which contains everything needed to run aan application as a container. |
| Docker Container | A running Docker Image is a Docker container. Multiple containers can be created from a Docker Image. |
| Docker Hub | Docker Hub is the github for docker images and containers. |
|Docker Repository| Docker repository is similar to a git repository but for docker images. |


## Getting Started with Docker
1. Install Docker Engine for you operating system by following the instructions [here](https://docs.docker.com/engine/install/).
2. Once docker is installed you can pull docker images from hub.docker.com by executing `docker pull <docker-image-name>`
3. To run a docker image, execute `docker run -itd <docker-image-name>`. This will run the docker container in the background.
4. Execute `docker ps` to ensure if the docker image is running or not.
    The Output will be similar to
    ```
    CONTAINER ID IMAGE  COMMAND CREATED        STATUS            PORTS NAMES
    30986b73dc00 ubuntu "bash"  45 minutes ago Up About a minute elated_franklin
    ```
5. Note the CONTAINER_ID as it may vary for every docker container.
6. Once the docker container is running, You can copy your code to the running container by executing `docker cp <Path-to-Your-Files> <CONTAINER-ID>:/
`. This will copy the files to the root directory of the container. You can copy scripts to automate the installation of the required dependencies in the docker.
5. You can execute your scripts by
   1. `docker exec -it <CONTAINER-ID> /bin/bash chmod +x <script-name>.sh`. This makes your script executable.
   1. `docker exec -it <CONTAINER-ID> /bin/bash ./<script-name>.sh`. This will execute your script.
6. Save your changed in the docker image by executing `docker commit <CONTAINER-ID> <docker-image-name>`
7. Now tag you docker image with a different name. Execute 
  `docker tag <docker-image-name> <username>/<repo>`
8. Finally Execute `docker push <username>/<repo>` to push to your changes to your remote repository.

Note: Before executing `docker push`, you must create an account in hub.dockerhub.com. After creating an account execute `docker login` in the terminal and type your username and password to authenticate yourself.
